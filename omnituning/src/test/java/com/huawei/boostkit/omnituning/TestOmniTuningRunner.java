/*
 * Copyright (C) 2020-2023. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.boostkit.omnituning;

import com.huawei.boostkit.omnituning.configuration.TestConfiguration;
import com.huawei.boostkit.omnituning.exception.OmniTuningException;
import org.junit.Test;

public class TestOmniTuningRunner extends TestConfiguration {
    @Test
    public void testRunOmniTuningRunner() {
        OmniTuningContext.initContext();
        OmniTuning.main(new String[] {"2020-09-02 00:00:00", "2020-09-02 00:00:00", "user", "pass"});
    }

    @Test(expected = OmniTuningException.class)
    public void testErrorNumberParams() {
        OmniTuning.main(new String[] {"2020-09-02 00:00:00", "2020-09-02 00:00:00"});
    }

    @Test(expected = OmniTuningException.class)
    public void testErrorTimeParser() {
        OmniTuning.main(new String[] {"2020-09-02 00-00-00", "2020-09-02 00-00-00", "user", "pass"});
    }

    @Test(expected = OmniTuningException.class)
    public void testErrorTimeOrder() {
        OmniTuning.main(new String[] {"2020-09-02 20:00:00", "2020:09:02 00-00-00", "user", "pass"});
    }
}
